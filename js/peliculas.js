document.addEventListener("DOMContentLoaded", function () {
    const btnBuscar = document.getElementById("btnBuscar");

    btnBuscar.addEventListener("click", function () {
        const txtNombre = document.getElementById("txtNombre").value.trim();

        if (txtNombre == "") {
            alert("Ingrese el nombre de la película a buscar...");
            return;
        }

        fetch(`https://www.omdbapi.com/?t=${txtNombre}&plot=full&apikey=30063268`)
            .then(respuesta => respuesta.json())
            .then(datos => {

                if (datos.Response == "False") {
                    alert("No existe dicha película...");
                    return;
                }

                if (datos.Title.toLowerCase() != txtNombre.toLowerCase()) {
                    alert("No existe dicha película...");
                    return;
                }

                document.getElementById("txtPelicula").innerHTML = datos.Title;
                document.getElementById("txtLanzamiento").innerHTML = datos.Year;
                document.getElementById("txtActores").innerHTML = datos.Actors;
                document.getElementById("txtResumen").innerHTML = datos.Plot;
                document.getElementById("imgPelicula").src = datos.Poster;
            })
            .catch(error => {
                console.error("Surgió un error al buscar los datos...", error);
                alert("Surgió un error...");
            });
    });
});
